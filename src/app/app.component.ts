import { Component, OnInit } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'db-websock';
  webSocketEndPoint: string = 'http://localhost:8080/ws';
  topic: string = "/topic/response";
  stompClient: any;
  messages: string[] = [];
  constructor() {

  }

  ngOnInit() {
    this.connect();
  }

  connect() {
    console.log("Initialize WebSocket Connection");
    let ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    const oThis = this;
    oThis.stompClient.connect({}, () => {
      oThis.stompClient.subscribe(oThis.topic, (sdkEvent: any) => {
        oThis.onMessageReceived(sdkEvent);
      });
      //oThis.stompClient.reconnect_delay = 2000;
    }, this.errorCallBack);
  }

  // on error, schedule a reconnection attempt
  errorCallBack(error: any) {
    console.log("errorCallBack -> " + error)
    setTimeout(() => {
      this.connect();
    }, 5000);
  }

  trigger() {
    this.stompClient.send("/app/trigger", {}, 'tigger-message Time: ' + new Date());
  }

  onMessageReceived(message: any) {
    console.log("Message Recieved from Server :: " + message);
    this.messages.push(message.body);
  }


}
